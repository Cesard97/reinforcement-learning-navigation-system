#! /usr/bin/env

import sys
import pybullet as pb
import pybullet_data
import time
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
import random
import numpy as np
import math as m
#import matplotlib.pyplot as plt
from qibullet import PepperVirtual
from qibullet import PepperRosWrapper
from qibullet import SimulationManager

# Iterations
MAX_IT = 70
# Boundaries
MAX_X = 7.0
MIN_X = -1.0
MAX_Y = 4.0
MIN_Y = -4.0
# Max episode
MAX_EPISODE = 200

class pepperTrainner():

    # ----------------------- Initialization -----------------------
    def __init__(self):
        # Create and launch simulation
        self.simulation_manager = SimulationManager()
        self.client = self.simulation_manager.launchSimulation(gui=True)
        self.simulation_manager.setLightPosition(self.client,[-5.0,0,1.45])
        # Create and spawn Pepper
        self.pepper = self.simulation_manager.spawnPepper(self.client, spawn_ground_plane=True)
        self.pos = [0, 0, 0]
        # ROS Wrapper
        #self.ros_wrapper = PepperRosWrapper()
        #self.ros_wrapper.launchWrapper(self.pepper, "/naoqi_driver")
        # Q-Learning
        self.Qtable = np.zeros((156,5))
        #self.Qtable = np.genfromtxt('qTable3.csv',delimiter=',')
        self.score = []
        self.endCause = [0, 0, 0]

    def restart_sim(self):
        # Create and launch simulation
        self.simulation_manager = SimulationManager()
        self.client = self.simulation_manager.launchSimulation(gui=True)
        self.simulation_manager.setLightPosition(self.client,[-5.0,0,1.45])
        # Create and spawn Pepper
        self.pepper = self.simulation_manager.spawnPepper(self.client, spawn_ground_plane=True)


    def main(self):
        # ----------------------- Main Loop -----------------------
        episode = 0
        epsilon = 0.5
        while episode < MAX_EPISODE:
            print('--------------- EPISODE ' + str(episode) + ' ---------------')
            # ----------------------- Init Episode -----------------------
            if episode != 0:
                self.restart_sim()
            # Episode variables
            step = 0
            # Conditions to terminate episode
            endEpisode = False
            outOfBounds = False
            maxItReached = False
            goalReached = False
            # Score
            totalScore = 0
            # Update epsilon
            epsilon -= 0.01
            if epsilon < 0.05:
                epsilon = 0.05
            print('Epsilon = ' + str(epsilon))
            # Subscribing to the bottom RGB camera
            self.pepper.subscribeCamera(PepperVirtual.ID_CAMERA_TOP)
            # Spawn human head randomly
            sphereX = 2+(np.random.randint(10))/2
            sphereY = (np.random.randint(16) + 2*MIN_Y)/2
            pb.setAdditionalSearchPath(pybullet_data.getDataPath())
            sphereUrdf = pb.loadURDF('sphere2red.urdf', [2.0,0.5,1.45], globalScaling=0.25)
            time.sleep(1)
            # Update state
            newState = self.update_state()
            # Print state
            if newState[0] == 0:
                print('(State) LOST')
            else:
                print('(State) Area: ' +str(newState[0]) + ', Angle: ' + str(10*newState[1]))

            # ----------------------- Episode Loop -----------------------
            while not endEpisode:
                # Epsilon-greedy movement
                action = self.action_selection(newState, epsilon)
                self.move(action)
                # Update state
                prevState = newState
                newState = self.update_state()
                if newState[0] == 0:
                    print('(State) LOST')
                else:
                    print('(State) Area: ' +str(newState[0]) + ', Angle: ' + str(newState[1]))
                # Get reward
                reward = self.get_reward(prevState,newState)
                # Verify if max iterations reached
                if (step > MAX_IT):
                    maxItReached = True
                    print('Episode terminated: Max iterations reached')
                    self.endCause[0] += 1
                # Verify if robot out of bounds
                if (self.pos[0]>MAX_X) or (self.pos[1]>MAX_Y) or (self.pos[0]<MIN_X) or (self.pos[1]<MIN_Y):
                    outOfBounds = True
                    print('Episode terminated: Robot out of bounds')
                    self.endCause[1] += 1
                    reward = -5
                # Verify if robot reach goal
                if (newState[0] > 40) and (newState[1] == 0):
                    goalReached = True
                    print('Episode terminated: Robot reached goal')
                    self.endCause[2] += 1
                    reward = 50 - (step*0.5)
                print('(Reward) r: ' +str(reward))
                totalScore += reward
                # End episode condition
                endEpisode = maxItReached or outOfBounds or goalReached
                # Update Qtable
                self.update_Qtable(reward,action,prevState,newState)
                # Update step
                step += 1
                print('STEP: ' +str(step))

            # Results from episode
            episode += 1
            print('Saving results from episode...')
            np.savetxt('qTable9.csv', self.Qtable, delimiter=',')
            np.savetxt('score9.csv', self.score, delimiter=',')
            np.savetxt('terminationCause9.csv', self.endCause, delimiter=',')
            print('Results were saved')
            # Score
            self.score.append(totalScore)
            print('Acumulated Score: ' + str(self.score))
            # Reset episode
            self.simulation_manager.resetSimulation(self.client)
            self.simulation_manager.stopSimulation(self.client)

    # ----------------------- Functions -----------------------
    # ACTION
    def movement_action(self, action = 0, vel = 0.5):
        # Transforming action to movement
        if action == 0:
            self.pepper.move(vel,0.0,0.0)
        elif action == 1:
            self.pepper.move(0.0,vel,0.0)
        elif action == 2:
            self.pepper.move(0.0,-vel,0.0)
        elif action == 3:
            self.pepper.move(0.0,0.0,1.5708)
        elif action == 4:
            self.pepper.move(0.0,0.0,-1.5708)

    # Low level control
    def move(self, action):
        # Get previous position
        prevX, prevY, prevW = self.pepper.getPosition()
        self.movement_action(action)
        print('(Action) Pepper is moving: ' + str(action))
        time.sleep(1)
        # Update position
        self.pos = self.pepper.getPosition()
        count = 0
        while ((abs(prevX-self.pos[0]) < 0.1) and (abs(prevY-self.pos[1]) < 0.1) and (abs(prevW-self.pos[2]) < 0.1)) and (count < 5):
            self.movement_action(action)
            time.sleep(1)
            self.pos = self.pepper.getPosition()
            count += 1
        self.movement_action(0,0)
        time.sleep(0.1)
        # Print position
        print('Pepper stopped moving: ' +str(round(self.pos[0],1)) + ', ' + str(round(self.pos[1],1)) + ', ' + str(round(self.pos[2],1)))

    # STATE
    def update_state(self):
        # Get image and mask for the red sphere
        mask = self.get_mask()
        # Determine sqrt of the number of pixels
        size = round(m.sqrt(np.count_nonzero(mask)))
        # Minimun threshold
        if size > 9:
            # Determine position of the square
            wideLength = len(np.nonzero(mask)[1])
            center = round(wideLength/2)
            posX = np.nonzero(mask)[1][center]
            # Transform position to a discrete angle
            if posX < 65:
                angle = -2
            elif posX < 135:
                angle = -1
            elif posX < 185:
                angle = 0
            elif posX < 250:
                angle = 1
            else:
                angle = 2
            # Return state
            return [size,angle]
        else:
            # Return if lost
            return [0,0]

    # Get mask
    def get_mask(self):
        # Retrieving and displaying the synthetic image using OpenCV
        img = self.pepper.getCameraFrame()
        #cv2.imshow("Synthetic top camera", img)
        #cv2.waitKey(600)
        # Adding a mask to image to get red pixels
        lower = np.array([0,0,100])
        upper = np.array([150,150,255])
        mask = cv2.inRange(img,lower,upper)
        #cv2.imshow("Top camera mask", mask)
        #cv2.waitKey(600)

        return mask

    # REWARD
    def get_reward(self, prevState, newState):
        reward = 0
        # Tope de estado
        if prevState[0] > 40:
            prevState[0] = 40
        if newState[0] > 40:
            newState[0] = 40
        # Reward for not changing state
        if newState == prevState:
            reward = 0
        else:
            # Reward for centering the objective
            if (abs(newState[1]) < abs(prevState[1])):
                reward = 2
            elif (abs(newState[1]) > abs(prevState[1])) and prevState != [0,0]:
                reward = -3
            # Reward for getting closer to the objective
            if (newState[0] > prevState[0]):
                reward += 1
            elif (newState[0] < prevState[0]):
                reward += -2
            # Reward for getting to objective or getting lost
            if (newState == [0,0]) and (prevState != [0,0]):
                reward = -5

        return reward

    # Q-TABLE
    def update_Qtable(self, reward, action, prevState, newState, gamma=1, alpha=0.2):
        prevIndex = self.get_index(prevState)
        newIndex = self.get_index(newState)

        deltaQ = reward + gamma*np.amax(self.Qtable[newIndex]) - self.Qtable[prevIndex][action]
        self.Qtable[prevIndex][action] +=  alpha*deltaQ

    # GREEDY ACTION SELECTION
    def action_selection(self, state, epsilon = 0.5):
        if random.random() < epsilon:
            return np.random.randint(5)
        else:
            index = self.get_index(state)
            return np.argmax(self.Qtable[index])

    def get_index(self, state):
        if state == [0,0]:
            index = 0
        else:
            if state[0] > 40:
                state[0] = 40
            index = (state[0]-10)*5 + (state[1]+3)

        return index


if __name__ == "__main__":
	pepper_trainner = pepperTrainner()
	pepper_trainner.main()
