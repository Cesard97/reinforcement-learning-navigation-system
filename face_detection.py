#! /usr/bin/env python

# Libraries
import sys
import rospy
import rospkg
sys.path.remove('/opt/ros/kinetic/lib/python2.7/dist-packages')
import cv2
from robot_toolkit_msgs.msg import *
from robot_toolkit_msgs.srv import *
from std_msgs.msg import *
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np


# Face Detection Class
class FaceDetection:
    # -------------------------------------------------------------------------
    #                                  INIT
    # -------------------------------------------------------------------------
    def __init__(self):
        # CONSTANTS
        FACEPATH = "/home/cesar/.local/lib/python3.5/site-packages/cv2/data/haarcascade_frontalface_alt2.xml"
        # Declare face classifier
        self.face_cascade_clasifier = cv2.CascadeClassifier(FACEPATH)
        # Atributes
        self.state =[0,0]
        self.faces = []
        self.img = []
        self.cv_bridge = CvBridge()
        self.message = Int32()

        # Camera service
        self.msg = vision_tools_msg()
        self.msg.camera_name = "front_camera"
        self.msg.command = "custom"
        self.msg.frame_rate = 1
        self.msg.resolution = 2
        self.msg.color_space = 11
        print('Waiting for vision tools service...')
        rospy.wait_for_service('/robot_toolkit/vision_tools_srv')
        try:
            camera = rospy.ServiceProxy('/robot_toolkit/vision_tools_srv', vision_tools_srv)
            service = camera(self.msg)
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)
        # Setting parameters
        self.msg.command = "set_parameters"
        self.msg.camera_parameters.brightness = 10 # original 0
        self.msg.camera_parameters.contrast = 30
        self.msg.camera_parameters.saturation = 64
        self.msg.camera_parameters.hue = 0
        self.msg.camera_parameters.horizontal_flip = 0
        self.msg.camera_parameters.vertical_flip = 0
        self.msg.camera_parameters.auto_exposition = 1
        self.msg.camera_parameters.auto_white_balance = 1
        self.msg.camera_parameters.auto_gain = 1
        self.msg.camera_parameters.reset_camera_registers = 0
        self.msg.camera_parameters.auto_focus = 1
        self.msg.camera_parameters.compress = True
        self.msg.camera_parameters.compression_factor = 30
        try:
        	camera = rospy.ServiceProxy('/robot_toolkit/vision_tools_srv', vision_tools_srv)
        	service = camera(self.msg)
        except rospy.ServiceException as e:
        	print("Service call failed: %s"%e)


    # -------------------------------------------------------------------------
    #                                  MAIN
    # -------------------------------------------------------------------------
    def main(self):
        #  Node init
        rospy.init_node('face_detection', anonymous=False)
        rate = rospy.Rate(10)
        # Subscribers
        rospy.Subscriber('/robot_toolkit_node/camera/front/image_raw', Image, self.cameraCallback)
        # Publishers
        self.statePub = rospy.Publisher('state', Int32, queue_size = 100)

        # Main loop
        while not rospy.is_shutdown():
            # Face detection
            self.detect_faces()
            # Print actual state
            if self.state[0] == 0:
                print('(State) LOST')
            else:
                print('(State) Area: ' +str(self.state[0]) + ', Angle: ' + str(10*self.state[1]))
            # Publish actual state index
            self.message.data = self.get_index(self.state)
            self.statePub.publish(self.message)
            rate.sleep()

        cv2.destroyAllWindows()


    # -------------------------------------------------------------------------
    #                               FUCNTIONS
    # -------------------------------------------------------------------------

    # Detects faces given the image from Pepper's camera
    def detect_faces(self):
        # Detects if image
        if len(self.img) != 0:
            # Use of classifier to detect faces
            self.faces = self.face_cascade_clasifier.detectMultiScale(self.img, 1.3,5)
            #self.faces1 = self.faceCascade1.detectMultiScale(self.img, 1.3,5)  #parametros 1.3,5
            #self.profile = self.profileCascade.detectMultiScale(self.gray, 1.1,5)  #parametros 1.1,5

            # No faces
            if len(self.faces):
                self.state = [0,0]
            # If more than one face takes the closest face for reference
            else:
                for (x,y,w,h) in self.faces:
                    cv2.rectangle(self.img, (x,y), (x+w,y+h), (255,0,0),2)
                    posX = x+w/2
                    # Transform position in x to a discrete angle
                    if posX < 65:
                        angle = -2
                    elif posX < 135:
                        angle = -1
                    elif posX < 185:
                        angle = 0
                    elif posX < 250:
                        angle = 1
                    else:
                        angle = 2
                    # Update state
                    self.state[0] = w
                    self.state[1] = angle
        else:
            self.state = [0,0]

    # Returns the index of a State given the size of the rectangle and its xpos
    def get_index(self, state):
        if state == [0,0]:
            index = 0
        else:
            if state[0] > 40:
                state[0] = 40
            index = (state[0]-10)*5 + (state[1]+3)

        return index
    # -------------------------------------------------------------------------
    #                                CALLBACKS
    # -------------------------------------------------------------------------

    def cameraCallback(self, frontData):
        image = None
        imageFrontCamera = None
        if (frontData.encoding == 'compressed bgr8'):
        	frame = np.frombuffer(frontData.data, dtype='uint8')
        	image = cv2.imdecode(frame, 1)
        else:
        	image = self.cv_bridge.imgmsg_to_cv2(frontData, "bgr8")

        if (image is not None):
        	timestamp = datetime.now()
        	self.img = image


if __name__ == "__main__":
	faceDetection = FaceDetection()
	faceDetection.main()
